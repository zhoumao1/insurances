// 兼容 START
import 'babel-polyfill'
import Es6Promise from 'es6-promise'
// 兼容 END -->Es6Promise.polyfill()

import Vue from "vue";
import App from "./App.vue";
import router from "./router/router-index";
import store from "./store";

import "./styles/main.less";
import "./styles/animated.less";

import "amfe-flexible";
import '@/mixins/global.mix';

import { installPlugin } from '@/plugins/plugins-index'

Es6Promise.polyfill()
installPlugin(Vue, {
	router
})


Vue.config.productionTip = false;

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount("#app");

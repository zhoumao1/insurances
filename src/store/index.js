import Vue from "vue";
import Vuex from "vuex";
import getters from './getters'
import apiAuth from "./module/apiAuth.js";
import appInfoStore from "./module/app-info.store";
import createPersistedState from "vuex-persistedstate"
Vue.use(Vuex);

export default new Vuex.Store({
	state: {},
	mutations: {},
	actions: {},
	plugins: [createPersistedState({
		storage: window.sessionStorage
	})],
	modules: {
		apiAuth,
		appInfoStore
	},
	getters
});

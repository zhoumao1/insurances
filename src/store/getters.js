const getters = {
	isAppEntry: state => state.apiAuth.isAppEntry,
	homeRoute: state => state.apiAuth.homeRoute,
	hasAuth: state => state.apiAuth.hasAuth,
	/** @return {LoginDataProp|*} */
	loginData: state => state.apiAuth.loginData,
	redirectQueryStr: state => state.apiAuth.redirectQueryStr,
	code: state => state.apiAuth.code,
	code_type: state => state.apiAuth.code_type,
}
export default getters

import { decrypt, encrypt } from '@/houtai/api';
const state = {
	/**@type {{secret: string, appid: string}}*/
	appEnv: {}
}
const mutations = {
	M_APPENV(state, str){
		state.appEnv = str
	}
}

const getters = {
	G_APPENV({ appEnv }){
		let o = {}
		for (const appEnvKey in appEnv) {
			o[appEnvKey] = decrypt(appEnv[appEnvKey])
		}
		return o
	}
}

const actions = {
	SetAPPENVConfig({ commit }){
		let appChannel = sessionStorage.getItem('appChannel')
		let obj = {
			secret: appChannel === 'mini_11' ? process.env.VUE_APP_PD_API_SECRET:process.env.VUE_APP_WSK_API_SECRET,
			appid: appChannel === 'mini_11' ? 'com.pd.patient':'com.wsk.patient'
		}
		for (const objKey in obj) {
			obj[objKey] = encrypt(obj[objKey])
		}
		commit('M_APPENV', obj)
	}
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}

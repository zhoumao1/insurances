/**
 * 绑定 after-leave 事件在 Vue 的子实例中, 确保动画结束后可以执行
 *
 * @param {Vue} instance Vue instance.
 * @param {Function} callback callback of after-leave event
 * @param {Number} speed the speed of transition, default value is 300ms
 * @param {Boolean} once weather bind after-leave once. default value is false.
 */
/**
 * 绑定 after-leave 事件在 Vue 的子实例中, 确保动画结束后可以执行
 *
 * @param {Vue} instance Vue instance.
 * @param {Function} callback callback of after-leave event
 * @param options    { { event: string, speed: number, once: boolean } }
 * @property options.event 过渡结束后触发的事件名
 * @property options.speed 过渡速度，默认值为220ms
 * @property options.once  是否只绑定一次事件. 默认值为false。
 */
export const afterLeave = (instance, callback, options = { event: '', speed: 220, once: false }) => {
	if(!instance || !callback) throw new Error('instance & callback is required');
	let called = false;
	const afterLeaveCallback = function () {
		if(called) return;
		called = true;
		if(callback) {
			callback.apply(null, arguments);
		}
	};
	if(options.event){
		if (options.once) {
			instance.$once(options.event, afterLeaveCallback);
		} else {
			instance.$on(options.event, afterLeaveCallback);
		}
	}else {
		if (options.once) {
			instance.$once('after-leave', afterLeaveCallback);
		} else {
			instance.$on('after-leave', afterLeaveCallback);
		}
		setTimeout(() => {
			afterLeaveCallback();
		}, options.speed + 50);
	}
};

/**
 * 深度合并对象, 并不是引用指针
 * @param objs
 * @return {null}
 */
export const deepMerge = (...objs) => {
	const result = Object.create(null)

	function isPlainObject(val) {
		const toString = Object.prototype.toString
		return toString.call(val) === '[object Object]'
	}

	objs.forEach(obj => {
		if (obj) {
			Object.keys(obj)
			.forEach(key => {
				const val = obj[key]
				if (isPlainObject(val)) {
					// 递归
					if (isPlainObject(result[key])) {
						result[key] = deepMerge(result[key], val)
					} else {
						result[key] = deepMerge(val)
					}
				} else {
					//  数组也要重新赋值  不然依然会引用到其他的
					if (Array.isArray(val)) {
						result[key] = [...val]
					} else {
						result[key] = val
					}
				}
			})
		}
	})
	return result
}

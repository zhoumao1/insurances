import { eventCenter } from './lib/wpm-event-center';

export class WebPostmessage {
	constructor() {
		/**@type {Bridge}*/
		this.bridge = window.bridge
		this.isMP = window.__wxjs_environment === 'miniprogram'
		this.isWX = this.bridge.ua.indexOf('micromessenger') > - 1
		this.wx = null
		this.baseRoute = null
		this.eventCenter = new eventCenter()
	}

	static getInstance() {
		if (!WebPostmessage.instance) {
			WebPostmessage.instance = new WebPostmessage();
		}
		return WebPostmessage.instance;
	}

	init(options){
		this.wx = options.wx
		this.baseRoute = options.base_route
		window.addEventListener('message', (e) =>{
			if(typeof e.data === 'object' && e.data.action){
				if(e.origin === location.origin)return
				let { action } = e.data
				e.data.push = (action, data = {}, origin = '*') =>{
					this.publish(action, data, origin, e.source)
				}
				this.eventCenter.emit(action, e.data)

			}

		}, false);

		window.addEventListener('load',  (e) =>{
			let flag = 'ready'
			this.publish(flag, {
				app_channel: this.baseRoute,
			})
			// 微信浏览器
			if(this.isWX){
				this.subscribe(flag, (e) =>{
					e.push('app_env', {
						app_channel: this.baseRoute,
					})
					this.setStor('platformType', this.baseRoute)
				})
			}else if (this.isMP || location.href.includes('platform_type=') || location.href.includes('app_type=')){
				// 小程序
				this.setMiniplatformType()
			}else {
				// app
				this.setClientChannel()
			}
		}, false);
	}

	setStor(k, v){
		sessionStorage.setItem(k, v)
	}

	setMiniplatformType(){
		// 主要用 platform_type 原先 app_type 废弃
		let index = location.href.indexOf('platform_type')
		let appType
		let prefix = 'platform_type='
		if(index <= -1){
			index = location.href.indexOf('app_type')
			prefix = 'app_type='
		}
		if(index !== -1){
			let str = location.href.substr(index)
			let i = str.indexOf('&')
			if(i !== -1)appType = str.substring(prefix.length, i)
			else appType = str.substr(prefix.length)
		}
		this.eventCenter.emit('app_env', {
			app_channel: appType
		})
		this.setStor('platformType', appType)
	}

	setClientChannel(){
		let index = this.bridge.ua.lastIndexOf(' ')
		let appType = this.bridge.ua.substr(index).trim()
		this.eventCenter.emit('app_env', {
			app_channel: appType
		})
		this.setStor('platformType', appType)
	}

	/**
	 * 推送消息
	 * @param   {string} action
	 * @param   {Object} [data]
	 * @param   {string} [origin]
	 * @param arg
	 */
	publish(action, data = {}, origin = '*', ...arg){

		if(!data)data = {}
		let message = {
			action,
			origin,
			data,
		}

		if(arg[0]){
			arg[0].postMessage(message, origin);
			return;
		}

		if(this.isMP){
			this.wx.miniProgram.postMessage({ data: message })
			return
		}

		if(window.opener){
			window.opener.postMessage(message, origin);
		}else {
			window.parent.postMessage(message, origin);
		}

	}


	/**
	 * 订阅
	 * @param {'app_env'|string}   action
	 * @param {function} cb
	 */
	subscribe(action, cb) {
		this.eventCenter.off(action, cb)
		this.eventCenter.on(action, cb)
	}
}

/** @type {webPostmessage} */
export const webPostmessage = WebPostmessage.getInstance()

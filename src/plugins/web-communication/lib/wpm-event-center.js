import vue from 'vue'

export class eventCenter {
	// 通过事件类型作为属性来管理不通的事件回调
	constructor() {
		this.eventStack = {}
	}
	static getInstance() {
		if (!eventCenter.instance) {
			eventCenter.instance = new eventCenter();
		}
		return eventCenter.instance;
	}
	on(eventName, cb) {
		const { eventStack } = this
		const eventValue = eventStack[eventName]

		eventValue ? eventValue.push(cb) : eventStack[eventName] = [cb]
	}

	off(eventName, cb) {
		const { eventStack } = this
		const eventValue = eventStack[eventName]

		if (!eventValue) return

		eventValue.shift()
		// (eventValue || []).forEach((eventCb, index) => {
		// 	if (eventCb === cb) {
		// 		eventValue.splice(index, 1)
		// 	}
		// })
	}

	/**
	 * 分发事件
	 * @param {'default'|string} eventName
	 * @param data
	 */
	emit(eventName, data) {
		const { eventStack } = this
		const eventValue = eventStack[eventName]

		if (!eventValue) return
		(eventValue || []).forEach(eventCb => {
			eventCb(data)
		})
	}
}

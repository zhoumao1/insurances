import { webPostmessage } from './web-postmessage';

export default {
	install(Vue, options) {
		// let webPostmessage = new WebPostmessage
		webPostmessage.init(options)

		Vue.prototype.$PostMessage = webPostmessage;
	},
}


type ManagerOptions = {
	/** wx sdk */
	wx: object;

	/** 跟路由信息 */
	base_route: string;

};

type action = 'ready' | 'app_env' | string;

export interface WebPostmessageInstance{
	// constructor(options: ManagerOptions)

	/** 发布 消息类型 */
	publish(action: action, data?: {}|object, origin?: '*'|string): void;

	/** 订阅 消息 */
	// @ts-ignore
	subscribe(action: action, cb: Function): void;
}


declare module 'vue/types/vue' {
	interface Vue {
		$PostMessage: WebPostmessageInstance
	}
}
export const webPostmessage: {
	/** 初始化 消息通道 */
	init(options: ManagerOptions): void;
}

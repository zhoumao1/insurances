import { LoginByWechatFoH5UnionID, LoginByWechatForH5 } from '@/api/wx';

import { baseRoute } from '@/settings';
import store from '@/store';
import { Toast } from 'vant';
import { appEnv } from '@/libs/app-env/app-env';
import { off, on } from 'vue-blur-load/src/utils';
import { RouteManager } from 'vue-route-manager/src/vue-route-manager';

const once = function (el, event, fn) {
	let listener = function () {
		if (fn) {
			fn.apply(this, arguments);
		}
		off(el, event, listener);
	};
	on(el, event, listener);
};

class WeChatAuthCenter {
	constructor(router) {
		if (!WeChatAuthCenter.instance) {
			WeChatAuthCenter.instance = this;
		}

		/** @type {VueRouter} */
		this.$router = router;

		let flag = appEnv.getEnv();
		if (!(flag.useragent.is_wx && !flag.code && !flag.code_type))
			return WeChatAuthCenter.instance;


		this.isLocal = ['build_dev', 'dev', 'test', 'prod'].find((item) => item === process.env.VUE_APP_RUN_MODE);

		this._authAfterPath = '/wx_auth_center';

		this.$router.addRoute(this._genAuthAfterPage());

		this.$RouteManager = RouteManager.instance
		this.$router.beforeEach((to, from, next) => {
			// alert('to -->'+to.fullPath+' |----| '+to.matched.length+'|----|'+to.name)
			// alert('from -->'+from.fullPath+' |----| '+from.matched.length+'|----|'+from.name)
			// this._log(to, from)

			// HUAWEI 授权后退出会返回空白页
			if (this.isFirstEnter(to) && this.isFirstEnter(from)) {
				this.$router.back()
				next(false)
				return;
			}

			if (!this._getStorage('first_url')) this._setStorage('first_url', to.fullPath);

			// debugger
			if (to.meta['is_login'] || to.query._is_login) {
				// true: 授权之后再进入页面, false: 进入页面后 跳转时再判断是否授权
				this._setStorage('is_auth_before_enter', !!(this.isFirstEnter(from) && to.name));

				// TODO: 如果已绑定微信， 需要调整为 token， 否则使用 unionid
				if (this._getLoginData().unionid) {
					next();
				} else {
					if (this.$RouteManager.behavior.mode === 'replace' && from.fullPath === this._getStorage('first_url')) {
						this._setStorage('first_url', to.fullPath);
						this._setStorage('r_url', to.fullPath);
						this._reload(to.fullPath);
						next(false);
						return;
					}

					this._requestAuth(to);
					next(false);
				}
			} else {
				next();
			}
		});

		return WeChatAuthCenter.instance;
	}

	_log(to, from) {
		if (appEnv.getEnv().useragent.is_devtool) {
			[
				{ name: 'To', data: to },
				{ name: 'From', data: from }
			]
				.filter((_) => _.data)
				.forEach(({ name, data }) => {
					console.log(
						`%c WeChat Auth %c ${ name }：${ data.name } `,
						'background: #35495e; padding: 1px 4px; border-radius: 3px 0 0 3px;  color: #fff',
						'background: #41b883; padding: 1px; margin-left: -4px; border-radius: 0 3px 3px 0;  color: #fff',
						{
							path: to.path,
							fullPath: to.fullPath,
							meta: to.meta
						}
					);
				});
		} else {
			console.groupEnd();
			console.group('WechatAuth Log');
			console.log(to);
			console.log(from);
			console.groupEnd();
		}
	}

	isFirstEnter(from) {
		let { fullPath, path, name, matched } = from;
		return fullPath === '/' && path === '/' && !matched.length && name === null;
	}

	// TODO: 待完成
	login({ $route, $store }, cb) {
		if (this._getLoginData().unionid) {
			cb && cb();
		} else {
			this.$router.push({
				name: $route.name,
				query: {
					_is_login: true
				}
			});

			once(window, 'pageshow', () => {
				if (this._getLoginData().unionid) cb && cb();
			});
		}
	}

	_reload(path) {
		location.replace(location.origin + '/' + baseRoute + path);
	}

	/**
	 * @param {Route|{}}  route
	 */
	_requestAuth(route) {
		Toast.loading({ duration: 1500, forbidClick: true });
		let { fullPath } = route;
		const REDIRECT_URL = encodeURIComponent(
			location.origin + '/' + baseRoute + this._authAfterPath
		);
		this._setStorage('r_url', fullPath);
		let state, wxAuthURL;

		if (!this.isLocal) {
			let appid;
			if (process.env.VUE_APP_RUN_MODE === 'build_prod') {
				appid = 'wx83105b4f6d8eaf56'; //打包线上
			} else if (process.env.VUE_APP_RUN_MODE === 'build_test') {
				appid = 'wx16f2fcc1304fa0f1'; //打包测试
			}
			state = 'state';
			wxAuthURL =
				'https://open.weixin.qq.com/connect/oauth2/authorize' +
				'?appid=' +
				appid +
				'&redirect_uri=' +
				REDIRECT_URL +
				'&response_type=code' +
				'&scope=snsapi_userinfo' +
				'&state=' +
				state +
				'' +
				// 'connect_redirect=1'+
				'#wechat_redirect';
		} else {
			state = '';
			wxAuthURL =
				'https://share.ishenzang.com/mp/BaseMP/RequestWechatOAuth?nextURLID=' +
				REDIRECT_URL +
				'&routeParam=' +
				state;
		}

		// console.log(wxAuthURL)
		location.replace(wxAuthURL);
	}

	/**
	 * @return {LoginDataProp}
	 */
	_getLoginData() {
		let o = JSON.parse(sessionStorage.getItem(baseRoute + '_vuex'));
		if (o) {
			return o.apiAuth.loginData;
		}
		return store.getters.loginData;
	}

	/**
	 * @typedef  {'r_url'|'first_url'|'is_auth_before_enter'} StorageKeys
	 */
	/**
	 *
	 * @param {StorageKeys} k
	 * @param {any}    v
	 * @private
	 */
	_setStorage(k, v) {
		let o = this._getStorage();
		o[k] = v;
		sessionStorage.setItem(
			'__' + baseRoute.toUpperCase() + '_WX_AUTH__',
			JSON.stringify(o)
		);
	}

	/**
	 * @private
	 * @param {StorageKeys}  [k]
	 * @return {Object<StorageKeys>}
	 */
	_getStorage(k) {
		let o = JSON.parse(
			sessionStorage.getItem('__' + baseRoute.toUpperCase() + '_WX_AUTH__')
		);
		o = o || {};
		if (k) return o[k];
		else return o;
	}

	_QS2QO(url) {
		if (!url) return {};
		let query = new URLSearchParams(new URL(url).search);
		let qo = {};
		for (let key of query.keys()) {
			qo[key] = query.get(key);
		}

		return qo;
	}

	_genAuthAfterPage() {
		let _self = this;
		return {
			name: 'wx-auth-center',
			meta: {
				title: '授权中，请稍后...'
			},
			path: this._authAfterPath,
			component: {
				name: 'wx-auth-center',
				functional: true,
				async beforeRouteEnter(to, from, next) {
					/**
					 * TODO:【微信登录优化】v3
					 *
					 * 1. 首次授权后再进入页面(HIGH 已处理)
					 * -----------------------------------------------------------
					 * 2. 进入页面后, 首页 replace 到需要授权的页面(HIGH 已处理)
					 * -----------------------------------------------------------
					 *
					 * 3. 进入页面后, 首页 push 到需要授权的页面(MEDIUM)
					 *
					 * -----------------------------------------------------------
					 *
					 * 4. 进入页面后, 其他页面 replace 到需要授权的页面(MEDIUM, 结合实际情况, 看看是否需要处理)
					 *
					 * -----------------------------------------------------------
					 *
					 * 5. 进入页面后, 其他页面 push 到需要授权的页面(MEDIUM, 结合实际情况, 看看是否需要处理)
					 *
					 * -----------------------------------------------------------
					 *
					 * 6. 如果是原地授权, 此时该怎么办(NORMAL)
					 * 例如: A原本不需要授权, 但是在做某个操作的时候需要先授权再做处理
					 */
						// 如果是 微信重定向 后进入此页面, 那么 to.fullPath === from.path && !from.name
					let isWechatDevTools = navigator.userAgent
							.toLowerCase()
							.includes('wechatdevtools');
					let { first_url, r_url } = _self._getStorage();
					let isFE = r_url.includes(first_url);

					// 当返回 临时微信授权页, 需要做些处理(处理 push 到需要授权的页面)
					if (!_self.isFirstEnter(from) && to.fullPath !== from.fullPath && to.path === _self._authAfterPath) {
						if (isFE) {
							window.HandleBack.closeH5Page()
						} else {
							// _self._log(to, from)
							_self.$router.go(-1)
						}
						next(false);
						return;
					}

					if (store.getters.loginData.unionid) {
						// next(_self._getStorage('r_url'));
						_self.$router.go(-1)
						next(false)
						return;
					}
					let { query } = to;
					let loginInfo, unionid, openid;
					Toast.loading({ duration: 0, forbidClick: true });
					if (!_self.isLocal) {
						let { data } = await LoginByWechatForH5({
							code: query.code,
							code_type: query.code_type
						});
						unionid = data['unionid'];
						openid = data['openid'];
						loginInfo = data;
					} else {
						let { data } = await LoginByWechatFoH5UnionID({
							unionid: query.unionid
						});
						unionid = query.unionid;
						openid = query.openid;
						loginInfo = data;
					}

					store.commit('M_LOGIN_DATA', Object.assign(loginInfo, { unionid, openid }));

					Toast.clear(true);
					// debugger
					// 这块只能在手机上测, 模拟器上不好调试
					if (isFE) {
						_self.$router.replace(_self._getStorage('r_url'))
					} else {
						// TODO: 待完成
						if (r_url.includes('_is_login=true')) {
							if (isWechatDevTools) {
								_self.$router.go(-2);
							} else {
								_self.$router.back();
							}
						} else {
							_self.$router.replace(_self._getStorage('r_url'));
							/*setTimeout(() =>{
								_self.$router.push(_self._getStorage('r_url'));
							}, 120)
							setTimeout(() =>{
								_self.$router.go(-1)
							}, 100)*/
						}
					}
					next(false);
				}
			}
		};
	}
}

export default {
	install(Vue, { router }) {
		Vue.prototype.$wechatAuth = new WeChatAuthCenter(router);
	}
};

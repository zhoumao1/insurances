import {Route} from "vue-router";

export interface WeChatAuthCenter {
	// (options: PopupOptions): Promise<PopupAction>;
	// alert(options: PopupOptions): Promise<PopupAction>;
	// confirm(options: PopupOptions): Promise<PopupAction>;
	// close(): void;
	// install(): void;
	// setDefaultOptions(options: PopupOptions): void;
	// resetDefaultOptions(): void;
	// picker(options: PickerOptions): Promise<{ v: string | number | {}, i: number }>|void;
	// datePicker(options: DatePickerOptions): Promise<Date>|void;
	// login(route: Route, next: Function)
}

declare module 'vue/types/vue' {
	interface Vue {
		$wechatAuth: WeChatAuthCenter;
	}
}

import {VueCache, getIndexByKey, getStack} from './components/VueCache';
import mixin from './mixin';
import history from './history';
import config from './config/config';

function hasKey(query, keyName) {
	return !!query[keyName];
}


function getKey(src) {
	return src.replace(/[xy]/g, function (c) {
		let r = (Math.random() * 16) | 0;
		let v = c === 'x' ? r : (r & 0x3) | 0x8;
		return v.toString(16);
	});
}

const routerCache = {};
const routerStack = new Map

// 注册插件
routerCache.install = function (Vue, {router, name = config.componentName, keyName = config.keyName}) {
	// 如没有 传入路由实例则报错提示
	if (!router) {
		throw Error('\n vue-router is necessary. \n\n');
	}
	// 创建组件
	Vue.component(name, VueCache(keyName, routerStack));


	Vue.prototype.$pageStack = {
		getStack
	};

	// 传入 原始路由进行改造
	mixin(router);



	function beforeEach(to, from, next) {
		//fix: 如果to 与 from 的key 相同则也需要重新更新 key
		// if (!hasKey(to.query, keyName) || to.query[keyName] === from.query[keyName]) {
		if (!routerStack.has(to.fullPath)) {
			routerStack.set(to.fullPath, getKey(config.keyValueSrc))

			/*to.query[keyName] = routerStack.get(to.name)
			let replace = history.action === config.replaceName || !hasKey(from.query, keyName);*/
			let replace = history.action === config.replaceName/* || !routerStack.has(from.query)*/
			to.meta.is_replace = replace

			next({
				...to,
				replace: replace
			});
		} else {
			// let index = getIndexByKey(to.query[keyName]);
			let index = getIndexByKey(routerStack.get(to.fullPath));
			if (index === -1) {
				to.params[keyName] = config.forwardName;
			} else {
				to.params[keyName] = config.backName;
			}
			next({params: to.params});
		}
	}

	// ensure it's the first beforeEach hook
	router.beforeHooks.unshift(beforeEach);
};

export default routerCache;

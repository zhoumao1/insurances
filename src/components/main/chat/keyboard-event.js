let availHeight = Math.max(document.documentElement.clientHeight || document.body.clientHeight);
let inputs = document.getElementsByTagName('input');
let textareas = document.getElementsByTagName('textarea');
let footer = document.querySelector('#footer');
let ftStyle = footer.style;
let body = document.body;
let keyboardHeight;
// 绑定事件
for (let i = 0; i < inputs.length; i ++) {
	let input = inputs[i];
	input.onfocus = focused;
	input.onblur = blured;
}

for (let i = 0; i < textareas.length; i ++) {
	let textarea = textareas[i];
	textarea.onfocus = focused;
	textarea.onblur = blured;
}


function getOffsetTop(el) {
	let mOffsetTop = el.offsetTop;
	let mOffsetParent = el.offsetParent;
	while (mOffsetParent) {
		mOffsetTop += mOffsetParent.offsetTop + mOffsetParent.clientTop;
		mOffsetParent = mOffsetParent.offsetParent;
	}
	return mOffsetTop;
}

// 是否需要上移输入框
function needPullUpScreen(target, top, height) {
	let keyboardShow = false;
	let nodeName = target.nodeName;
	let leftHeight;
	let isAndroid = navigator.userAgent.indexOf('Android') > -1 || navigator.userAgent.indexOf('Adr') > -1;
	if (isAndroid) {
		leftHeight = availHeight - keyboardHeight;
	} else {
		leftHeight = availHeight - keyboardHeight * window.devicePixelRatio;
	}

	if (nodeName) {
		if ((top + height + 5) >= leftHeight) {
			switch (nodeName.toLowerCase()) {
				case 'input':
					keyboardShow = target.type !== 'button' && target.type !== 'file';
					break;
				case 'textarea':
					keyboardShow = true;
					break;
				default:
					keyboardShow = false;
					break;
			}
		}
	}
	return keyboardShow;
};

// 监听键盘弹出事件
window.addEventListener('native.keyboardshow', function (e) {
	// ftStyle.display = 'none';
	// console.warn(_this.$route)

	// 此处获取keyboard的高度，由插件提供，这里写死
	// keyboardHeight = e.keyboardHeight;
	keyboardHeight = 290;
	let activeEle = document.activeElement;
	console.log(activeEle, 'activeEle')

	// getBoundingClientRect 只在android 4.4以上才有用
	// top和height可用getOffsetTop(el)和el.offsetHeight替代
	// let boundingClientRect = getOffsetTop(footer);
	let boundingClientRect = activeEle.getBoundingClientRect();
	let top = boundingClientRect.top;
	let height = boundingClientRect.height;

	// 移到居中位置
	// 这个高度可以根据自己的情况来写
	let moveOffset = top + height - availHeight / 2;
	console.log(activeEle, 'activeEle')
	if (activeEle && needPullUpScreen(activeEle, top, height)) {
		body.style.webkitTransform = `translate3d(0, ${ - moveOffset }px, 0)`;
		body.style.transform = `translate3d(0, ${ - moveOffset }px, 0)`;
		body.style.webkitTransition = 'transform 200ms';
		body.style.transition = 'transform 200ms';
	}
});

// 监听键盘消失事件
window.addEventListener('native.keyboardhide', function () {
	body.style.webkitTransform = '';
	body.style.transform = '';
	body.style.webkitTransition = 'transform 200ms';
	body.style.transition = 'transform 200ms';

	setTimeout(function () {
		ftStyle.display = '';
	}, 1200);
});

// 模拟事件
function mockEvent(type, fn) {
	let createDiv = document.createElement('div');
	createDiv.style.display = 'none';
	document.body.appendChild(createDiv);

	let e = document.createEvent('MouseEvent');
	e.initEvent(type, true, true);

	createDiv.addEventListener(type, function () {
		fn && fn();
		createDiv.remove();
	});

	createDiv.dispatchEvent(e);
}

function focused() {
	// 事件模拟
	mockEvent('native.keyboardshow');
}

function blured() {
	mockEvent('native.keyboardhide');
}

import ParentView from '@/components/main/parent-view'

export default {
	path: '/dialog_chat',
	component: ParentView,
	children: [
		{
			path: '',
			name: 'dialog-chat',
			component: () => import('./dialog-chat'),
			meta: {
				is_login: true
			}
		},
	]
}

import axios from '@/api/index';
//开启对话 http://doc.shensx.com/project/22/interface/api/2621
export function OpenDialog(data) {
	return axios({
		method: 'post',
		url: '/micro/customerservice/microapi/CustomerServiceDialogApi/OpenDialog',
		data: { data }
	})
}
//分页获取聊天消息 http://doc.shensx.com/project/22/interface/api/2631
export function GetDialogMessagePageListByLT(params) {
	return axios({
		method: 'get',
		url: '/micro/customerservice/microapi/CustomerServiceDialogApi/GetDialogMessagePageListByLT',
		params
	})
}
//发送消息 http://doc.shensx.com/project/22/interface/api/2625
export function SendDialogMessage(data) {
	return axios({
		method: 'post',
		url: '/micro/customerservice/microapi/CustomerServiceDialogApi/SendDialogMessage',
		data: { data }
	})
}

import ParentView from '_c/main/parent-view'

export default {
	path: "/marketing/questions",
	component: ParentView,
	children: [
		{
			path: "register",
			name: 'register-info',
			component: () => import("@/views/questions/register-info/register-info"),
			meta: {
				title: '报名信息'
			}
		},
		{
			path: "evaluation",
			name: 'evaluation-form',
			component: () => import("@/views/questions/evaluation-form/evaluation-form"),
			meta: {
				title: '评测问卷'
			}
		},
		{
			path: "report",
			name: 'report',
			component: () => import("@/views/questions/report/report"),
			meta: {
				title: '评测问卷'
			}
		},
	]
}

import ParentView from '_c/main/parent-view'

export default {
	path: '/',
	component: ParentView,
	// redirect: '/index',
	children: [
		{
			path: '',
			name: 'insurance',
			component: () => import('@/views/insurance/insurance')
		},
		{
			path: 'v2',
			name: 'insurance-v2',
			component: () => import('@/views/pages/axyjk/axyjk'),
			meta: {
				is_login: true
			}
		},
	]
}

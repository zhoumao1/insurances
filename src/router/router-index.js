import Vue from 'vue';
import Router from 'vue-router';
import { miniProgramRouter, wechatFlag } from '@/libs/browser';
import store from '@/store';
import { asyncRoutes, routes } from '@/router/router-list';
import { baseRoute } from '@/settings';
import { handleUserAuth } from '@/router/auth-tool';
import { insuranceList } from '_v/insurance/insurance-list';
import { param2Obj } from '@/libs/tools';
import { RouteManager } from 'vue-route-manager/src/vue-route-manager';

Vue.use(Router);
const BASE_URL = baseRoute;
// 是否为app登录 有 code 并且 不是微信浏览器
const HAS_LOGIN_PARAMS = !!(param2Obj().code && param2Obj().code_type);
const APPLOGIN = (HAS_LOGIN_PARAMS || (store.getters.isAppEntry)) && !(wechatFlag().is_wechat);
// alert(APPLOGIN)
const createRouter = () => new Router({
  base: BASE_URL,
  mode: 'history',
  // 微信登录的话 使用 routes.concat(asyncRoutes)
  // APP登录 使用 asyncRoutes,  需动态添加 路由
  routes: APPLOGIN ? asyncRoutes : routes.concat(asyncRoutes)
});
export const router = createRouter();

export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

// 自定义 路由回退事件
const routerBack = router.back.bind(router);
router.back = () => {
  if (!RouteManager.getInstance(router).routePathList.length) {
    if (wechatFlag().is_mini_program) {
      miniProgramRouter.back();
    } else if (APPLOGIN) {
      window.parent === window && window.HandleBack.closeWebView();
    } else {
      // console.error('关闭h5', history.state)
      window.HandleBack.closeH5Page();
    }
  }

  routerBack();
};
// 全局路由拦截器
router.beforeEach(async (to, from, next) => {
  // console.warn(to, from, router, APPLOGIN, 'beforeEach')
  if (to.meta.title) {
    //判断是否有标题
    document.title = to.meta.title;
  }
  if (to.query.insur_code) {
    document.title = insuranceList.find(item => item.insur_code.toLowerCase() === to.query.insur_code.toLowerCase()).title;
  }
  // app登录 || 有token

  if (APPLOGIN) {
    // 安卓
    if (store.getters.hasAuth) {
      next();
    } else {
      if (param2Obj().code && param2Obj().code_type) {
        store.commit('M_CODE_OR_CODE_TYPE', {
          code: param2Obj().code,
          code_type: param2Obj().code_type
        });
      }
      // console.error('首次进入')
      // debugger
      // console.log(to.path, '初次进入的地址')
      store.commit('M_HOME_ROUTE', to);
      await store.dispatch('appLogin');

      next();
      // next({ ...to, replace: true })
    }
    return;
  }

  if (!wechatFlag().is_wechat) {
    alert('请使用微信打开');
    return;
  }

  // 保险介绍页 不需要授权   跳转时候 再授权
	// next()
	console.warn(to)
  if ((to.path.includes('insurances') || to.query.insur_code || to.path === '/') && to.query.insur_code !== 'axyjk') {
    next();
    return;
  }

	if(to.path.includes('v2')){
		next()
		return
	}

  // token 校验
  // 一般to.meta.requireAuth为true时才进行校验
  // 这里只有wxCallback路由不需要认证，其requireAuth===0
  if (to.meta.requireAuth !== 0) {
    // 如果状态中没有token，进入微信登录页面
    let hasToken = await handleUserAuth({
      full_path: to.fullPath,
      path: to.path
    });
    if (hasToken) {
      next();
    } else {
      store.commit('M_HOME_ROUTE', to);
    }
  } else {
    next();
  }
});

export default router;

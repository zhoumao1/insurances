import { baseRoute, title } from '@/settings';

let client = require('exceptionless').ExceptionlessClient.default;
export function monitorBug(error, data) {
  client.config.serverUrl = 'http://exception.shensx.com:5000';
  client.config.apiKey = 'Ag9c1suw2EVrNRB9HMMIkeFeDJ0qodi8XWqugItF';
  // let id = null;
  // if (store.state.user.user.userid) {
  //   id = store.state.user.user.userid
  // } else {
  //   id = 'null'
  // }
  client.createException(error)
    //设置事件的引用id，以便我们以后搜索它(参考号：id).如果您调用client.config.useReferenceIds();
    // .setReferenceId(id)
    // 添加order对象（排除特定字段的功能将在将来的版本中提供）。
    //.setProperty("Order", order)
    // Set the quote number.设置编号(扩展数据)
    .setProperty("Quote", data)
    // Add an order tag.添加标签
    //.addTags("Order")
    // Mark critical.标记为关键
    .markAsCritical()
    // Set the coordinates of the end user.设置用户地理位置
    //.setGeo(43.595089, -88.444602)
    // Set the user id that is in our system and provide a friendly name.设置系统中的用户id并提供友好的名称。
    .setUserIdentity(title, baseRoute)
    // Submit the event.提交
    .submit();
}

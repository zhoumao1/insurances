export class EAppEnv {

	static EPlatformInfo = (() => {
		let obj = {
			'com.wsk.patient': {
				name: '凯德尼患者端(小程序)',
				channel: 'Wechat_OA_KidneyService',
				type: '2'
			},
			'com.wsk.doctor': {
				name: '凯德尼医生端(APP)',
				channel: 'App_Kidney_Doctor',
				type: '2'
			},
			'com.ssx.patient': {
				name: '肾上线患者端(APP)',
				channel: 'TherapistPush',
				type: '1'
			},
			'com.pd.patient': {
				name: '腹透患者端(小程序)',
				channel: '',
				type: '4'
			},
			'com.pd.doctor': {
				name: '腹透医生端(APP)',
				channel: '',
				type: '2'
			},
			'com.pkiga.patient': {
				name: '北大IGA患者端(小程序)',
				channel: '',
				type: '3'
			},
			'com.pkiga.doc': {
				name: '北大IGA医生端(APP)',
				channel: '',
				type: '3'
			},
			'com.mrsystem.patient': {
				name: '病历系统(H5)',
				channel: ''
			},
			'com.minissx.patient': {
				name: '肾上线患者端(小程序)',
				channel: 'TherapistPush',
				type: '1'
			}
		}
		for (const key in obj) {
			obj[key].appid = key
		}
		return obj
	})()


	static getPlatformByAppid(id) {
		return this.EPlatformInfo[id]
	}

	// 兼容 只有 platform_type 却没有appid 的程序
	static getPlatformByType(type, is_wx_mini) {
		let arr = Object.values(this.EPlatformInfo)
			.filter(item => item.type === type)
			.map(item => this.getPlatformByAppid(item.appid))
		if(arr.length > 1){
			arr = arr.filter(item => is_wx_mini && item.name.includes('小程序'))
		}
		return arr.length && arr[0] || {
			type
		}
	}
}

/**
 * @typedef PlatformProps
 * @property   {string} appid
 * @property   {string} name  平台名
 * @property   {string<number>} type  平台类型
 * @property   {string} channel  平台来源(商城用)
 */

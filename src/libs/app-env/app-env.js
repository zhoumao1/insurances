import { EAppEnv } from '@/libs/app-env/e-app-env';
class AppEnv {
	constructor() {
		this.ua = navigator.userAgent;

		let { appid, platform_type, code, code_type, devtool } = this.query2Obj()
		let os = {
			is_android: (/(Android);?[\s/]+([\d.]+)?/.test(this.ua)),
			is_ios: !!this.ua.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
			// 微信h5
			is_wx: this.ua.toLowerCase().indexOf('micromessenger') > - 1,
			is_wx_mini: window['__wxjs_environment'] === 'miniprogram',
			is_devtool: devtool === 'kidney'
		}

		if(os.is_wx && !os.is_wx_mini)os.is_wx_h5 = true
		if(os.is_android || os.is_ios)os.is_phone = true
		if(!os.is_wx && os.is_phone){
			os.is_app = this.ua.match(/CustomAgent\/KidneyonlineApp/) || !(this.ua.match(/Chrome\/([\d.]+)/) || this.ua.match(/CriOS\/([\d.]+)/)) && !!this.ua.match(/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/)
			os.is_h5 = !os.is_app
		}

		let useragent = {}
		for (const k in os) {
			if(os[k])useragent[k] = os[k]
		}

		let platform
		if(appid){
			platform = EAppEnv.getPlatformByAppid(appid)
		}else if (platform_type){
			platform = EAppEnv.getPlatformByType(platform_type, useragent.is_wx_mini)
		}else {
			platform = {
				appid: '',
				name: '未知',
				type: 'unknown'
			}
		}

		let appEnvInfo = {
			code,
			code_type,
			useragent,
			platform,
			appid: appid || platform.appid
		}

		this.envkey = ('__'+location.pathname.split('/').filter(_ => _)[0]+'_env__').toUpperCase()
		this.setEnv(this.getEnv() || appEnvInfo)
	}

	query2Obj(url = location.search){
		const s = decodeURIComponent(url.split('?')[1])
			.replace(/\+/g, ' ')
		if (!s) {
			return {}
		}
		const o = {}
		const searchArr = s.split('&')
		searchArr.forEach(v => {
			const index = v.indexOf('=')
			if (index !== -1) {
				const name = v.substring(0, index)
				o[name] = v.substring(index + 1, v.length)
			}
		})
		return o
	}

	/** @return {AppEnvProps} */
	getEnv(){
		return JSON.parse(sessionStorage.getItem(this.envkey))
	}

	setEnv(data){
		/**@type {AppEnvProps}*/
		window[this.envkey] = data
		sessionStorage.setItem(this.envkey, JSON.stringify(data))
	}
}

export const appEnv = new AppEnv()

/**
 * @typedef AppEnvProps
 * @property   {string}   appid
 * @property   {string}   code      免授权登录码
 * @property   {string}   code_type
 * @property   {Object}   useragent 用户代理
 * @property   {boolean}   useragent.is_android
 * @property   {boolean}   useragent.is_ios
 * @property   {boolean}   useragent.is_app
 * @property   {boolean}   useragent.is_wx   是否为微信体系
 * @property   {boolean}   useragent.is_h5   是否为非微信体系下的H5页面
 * @property   {boolean}   useragent.is_wx_h5   是否为微信体系下的H5页面
 * @property   {boolean}   useragent.is_wx_mini
 * @property   {boolean}   useragent.is_devtool 是否为测试人员调试，URL查询字符串中需含有 devtool=kidney
 * @property   {PlatformProps}   platform 平台信息
 */

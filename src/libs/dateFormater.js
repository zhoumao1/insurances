import moment from "moment";
moment.locale('zh-cn')
//获取减8 --转换为日期
export function getTime(unix, format) {
    if (!format) {
        format = "YYYY-MM-DD";
    }
    if (unix < 1e10) {
        unix = (unix - 8 * 3600) * 1000;
    } else {
        unix = unix - 8 * 3600 * 1000;
    }
    return moment(unix).format(format);
}
//上传加8  --添加8小时时间戳
export function uploadTime(string) {
    return moment(string).unix() + 8 * 3600;
}

//时间戳转换 日期
export function stringFormater(string, format) {
    if (!format) {
        format = "YYYY-MM-DD";
    }
    return moment(string).format(format);
}

import axios from '@/api/index.js'

/**@see https://www.showdoc.cc/kidneytec?page_id=2477043668481634  微信登录 --线上*/
export function LoginByWechatForH5(data) {
	return axios({
		method: 'post',
		url: '/api/WxH5Api/LoginByWechatForH5',
		data: { data: data }
	})
}

/**@see https://www.showdoc.cc/kidneytec?page_id=2477043668481634 微信登录 --本地*/
export function LoginByWechatFoH5UnionID(data) {
	return axios({
		method: 'post',
		url: '/api/WxH5Api/LoginByWechatFoH5UnionID',
		data: { data: data }
	})
}

// 微信签名授权
export function GenerateShareFriendSignInfo(data) {
  return axios({
    method: 'post',
    url: '/api/WxH5Api/GenerateShareFriendSignInfo',
    data: { data: data }
  })
}

export function SubmitWeChatPayForh5(data) {
	return axios({
		method: 'post',
		url: '/api/WxH5Api/SubmitWeChatPayForh5',
		data: { data: data }
	})
}
